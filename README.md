### Python System for Data Processing

<p>This project implements a Python system for accessing and processing data from an API, transforming it with PySpark,
and loading it into a PostgreSQL database in a Docker container.</p>

#### Project Structure:

- api_client.py: Script to interact with the API and retrieve data.
- parquet_handler.py: Script to read and write data to Parquet files.
- pyspark_transformations.py: Script to perform data transformations using PySpark.
- postgres_loader.py: Script to load data into the PostgreSQL database.
- docker_utils.py: Utility functions for interacting with Docker containers.
- data/: Directory for storing data files (API responses, Parquet files).
- logs/: Directory for storing application logs.
- requirements.txt: List of required libraries and their versions.
- Dockerfile: Dockerfile for building the container.
- .env: File containing environment variables for sensitive information.
- .gitlab-ci.yml: GitLab CI/CD pipeline configuration file.

#### Libraries and Frameworks:

- requests: HTTP library for API interaction.
- pandas: Data manipulation library.
- pyarrow: Efficient Parquet library.
- pyspark: Spark API for Python.
- pyspark.sql: Spark SQL functionalities for data manipulation and analysis.
- psycopg2: PostgreSQL database driver.
- sqlalchemy: Object-relational mapping library for database interactions.
- docker: Docker client library for managing containers.

#### Project Setup:

- Install Python 3 and required libraries (refer to requirements.txt).
- Create a .env file and add environment variables for API credentials, database connection details, etc.
- Configure your API endpoint and database configuration in project scripts.
- Build the Docker image: docker build -t data_processing_image .
- Run the application:
    - python api_client.py (to retrieve data)
    - python parquet_handler.py (to process data)
    - python pyspark_transformations.py (to transform data)
    - python postgres_loader.py (to load data into database)

#### GitLab CI/CD Integration:

- Push your code to a GitLab repository.
- Configure the .gitlab-ci.yml file to automate the build, test, and deployment process.
- Define stages for API interaction, data processing, database loading, and optionally Docker build and deployment.
- Use artifacts to share data between stages.
- Schedule pipelines to run automatically on code changes or pushes.

#### Further Enhancements:

- Implement unit tests for individual modules.
- Add configuration management tools like YAML files for easier configuration updates.
- Integrate with CI/CD pipelines for automated builds and deployments.
- Add logging and error handling for improved monitoring and debugging.
