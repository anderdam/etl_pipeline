"""
Scope: Reads and writes data to/from Parquet files.

Implementation:

    Implement methods for reading and writing data frames and tables.
    Allow specifying compression options for Parquet files.
    Consider using additional libraries like pandas for further data manipulation.

Good practices:

    Use a dedicated class for Parquet operations to improve code organization.
    Handle potential errors during file read/write operations.
    Document the supported data formats and compression options.
    Consider adding support for other file formats like CSV or JSON.
"""

import pyarrow.parquet as pq


class ParquetHandler:
    def __init__(self, data_path):
        self.data_path = data_path

    def read_parquet(self):
        # Read data from a Parquet file
        table = pq.read_table(self.data_path)
        return table

    def write_parquet(self, data, compression="snappy"):
        # Write data to a Parquet file
        pq.write_table(table, self.data_path, compression=compression)


# Usage example
parquet_handler = ParquetHandler("data/data.parquet")
data = parquet_handler.read_parquet()

# Process data and write to a new Parquet file
transformed_data = ...
parquet_handler.write_parquet(transformed_data)
