"""
Scope: Interacts with the API, retrieves data, and parses the response.

Implementation:

    Implement methods for different API endpoints and data formats.
    Handle potential errors and HTTP status codes.
    Use appropriate libraries for parsing JSON, XML, or other data formats.

Good practices:

    Use a dedicated class for API interaction to encapsulate logic and facilitate reuse.
    Configure API access details with environment variables or a separate configuration file.
    Handle and log errors gracefully.

"""

import requests


class ApiClient:
    def __init__(self, api_url, api_key):
        self.api_url = api_url
        self.api_key = api_key

    def get_data(self):
        # Use the requests library to make an API request
        response = requests.get(
            self.api_url, headers={"Authorization": f"Bearer {self.api_key}"}
        )

        # Check for errors in the response
        if response.status_code != 200:
            raise RuntimeError(f"API request failed: {response.reason}")

        # Parse the response data (e.g., using JSON or XML libraries)
        data = response.json()

        return data


# Usage example
api_client = ApiClient("https://your_api_url.com", "your_api_key")
data = api_client.get_data()

# Process data further (e.g., save to file, handle errors)
