"""
Scope: Manages Docker container build and execution.

Implementation:

    Utilize the docker library to interact with Docker daemon.
    Implement methods for building Docker images from Dockerfiles.
    Consider methods for managing container logs, networks, and volumes.

Good practices:

    Use Docker Compose for managing multi-container applications.
    Store Dockerfile and configuration files in version control.
    Define environment variables for sensitive information like database credentials.

"""

import docker


class DockerUtils:
    def __init__(self, docker_client):
        self.docker_client = docker_client

    def build_image(self, image_name):
        # Build a Docker image from Dockerfile
        self.docker_client.images.build(path=".", tag=image_name)

    def run_container(self, image_name):
        # Run a Docker container from the specified image
        self.docker_client.containers.run(image_name)


# Usage example
docker_client = docker.from_env()
docker_utils = DockerUtils(docker_client)

docker_utils.build_image("data_processing_image")
docker_utils.run_container("data_processing_image")
